package account

import (
	"net/mail"
	"strings"
	"time"

	"github.com/labstack/echo"
	"gitlab.com/lightim/laser/controller/context"
	"gitlab.com/lightim/laser/errors"
	accountModel "gitlab.com/lightim/laser/model/account"
)

/* 注册帐号 */
func Signup(c echo.Context) error {
	ctx := c.(*context.Context)
	var req SignupRequest
	if err := ctx.Bind(&req); err != nil {
		return errors.ErrRequestInvalid
	} else if err := ctx.Validate(&req); err != nil {
		return errors.ErrRequestInvalid
	}

	if email, err := mail.ParseAddress(req.Email); err != nil {
		return errors.ErrEmailMalformed
	} else {
		req.Email = email.Address
	}

	if a, err := accountModel.GetAccountByEmail(req.Email); err != nil {
		return errors.ErrInternalException
	} else if a != nil {
		return errors.ErrEmailAlreadyExists
	}

	a, err := accountModel.CreateAccount(req.Email, req.Password, req.Nickname, req.Avatar)
	if err != nil {
		return errors.ErrInternalException
	}
	t, err := accountModel.CreateToken(a.Id, time.Time{})
	if err != nil {
		return errors.ErrInternalException
	}
	return ctx.SUCCESS(t)
}

/* 获取当前帐号的信息 */
func GetAccount(c echo.Context) error {
	ctx := c.(*context.Context)
	return ctx.SUCCESS(ctx.Account)
}

/* 登录 */
func Login(c echo.Context) error {
	ctx := c.(*context.Context)
	var req LoginRequest
	if err := ctx.Bind(&req); err != nil {
		return errors.ErrRequestInvalid
	} else if err := ctx.Validate(&req); err != nil {
		return errors.ErrRequestInvalid
	}

	var a *accountModel.Account
	var err error
	if strings.ContainsRune(req.Username, '@') {
		a, err = accountModel.GetAccountByEmail(req.Username)
	} else {
		a, err = accountModel.GetAccountByUsername(req.Username)
	}
	if err != nil {
		return errors.ErrInternalException
	} else if a == nil {
		return errors.ErrAccountNotFound
	}
	if !a.VerifyPassword(req.Password) {
		return errors.ErrPasswordIncorrect
	}

	t, err := accountModel.CreateToken(a.Id, time.Time{})
	if err != nil {
		return errors.ErrInternalException
	}

	return ctx.SUCCESS(t)
}

func SearchAccount(c echo.Context) error {
	ctx := c.(*context.Context)
	var req SearchAccountRequest
	if err := ctx.Bind(&req); err != nil {
		return errors.ErrRequestInvalid
	} else if err := ctx.Validate(&req); err != nil {
		return errors.ErrRequestInvalid
	}

	a, err := accountModel.SearchAccountByEmailOrUsername(req.Q)
	if err != nil {
		return errors.ErrInternalException
	}
	if len(a) == 0 {
		a = make([]*accountModel.Account, 0)
	}
	return ctx.SUCCESS(a)
}
