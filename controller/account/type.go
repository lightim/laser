package account

import (
	accountModel "gitlab.com/lightim/laser/model/account"
)

type SignupRequest struct {
	Email    string `json:"email" form:"email" query:"email" validate:"gt=0"`
	Nickname string `json:"nickname" form:"nickname" query:"nickname" validate:"gt=0"`
	Avatar   string `json:"avatar" form:"avatar" query:"avatar" validate:"gt=0"`
	Password string `json:"password" form:"password" query:"password" validate:"gt=0"`
}

type LoginRequest struct {
	Username string `json:"username" form:"username" query:"username" validate:"gt=0"`
	Password string `json:"password" form:"password" query:"password" validate:"gt=0"`
}

type CheckAccountByEmailRequest struct {
	Email string `json:"email" form:"email" query:"email" validate:"required,email"`
}

type CheckAccountByUsernameRequest struct {
	Username string `json:"username" form:"username" query:"username" validate:"gt=0"`
}

type SearchAccountRequest struct {
	Q string `json:"q" form:"q" query:"q" validate:"gt=0"`
}

type GetUserRequest struct {
	ID int64 `json:"id" form:"id" query:"id" validate:"gt=0"`
}

type UserRelationship struct {
	IsFriend bool `json:"is_friend"`
}

type GetUserResponse struct {
	*accountModel.Account
	Relationship *UserRelationship              `json:"relationship"`
	Setting      *accountModel.User2UserSetting `json:"setting"`
}
