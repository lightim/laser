package account

import (
	"github.com/labstack/echo"
	"gitlab.com/lightim/laser/controller/context"
	"gitlab.com/lightim/laser/errors"
	accountModel "gitlab.com/lightim/laser/model/account"
	friendModel "gitlab.com/lightim/laser/model/friend"
)

/* 获取用户信息 */
func GetUser(c echo.Context) error {
	ctx := c.(*context.Context)

	var req GetUserRequest
	if err := ctx.Bind(&req); err != nil {
		return errors.ErrRequestInvalid
	} else if err := ctx.Validate(&req); err != nil {
		return errors.ErrRequestInvalid
	}

	a, err := accountModel.GetAccountById(req.ID)
	if err != nil {
		return errors.ErrInternalException
	} else if a == nil {
		return errors.ErrAccountNotFound
	}

	uf, err := friendModel.GetUserFriendByUserIDAndFriendID(ctx.Account.Id, a.Id)
	if err != nil {
		return errors.ErrInternalException
	}
	setting, err := accountModel.GetUser2UserSetting(ctx.Account.Id, a.Id)
	if err != nil {
		return errors.ErrInternalException
	}

	a.Email = ""
	r := &GetUserResponse{
		Account: a,
		Relationship: &UserRelationship{
			IsFriend: uf != nil,
		},
		Setting: setting,
	}

	return ctx.SUCCESS(r)
}
