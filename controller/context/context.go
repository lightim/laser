package context

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo-contrib/session"
)

func (ctx *Context) getSessionToken() string {
	sess, _ := session.Get("session", ctx)
	if sess == nil {
		return ""
	}
	v, ok := sess.Values["token"]
	if !ok || v == nil {
		return ""
	}
	return v.(string)
}

func (ctx *Context) getBearerToken() string {
	auth := ctx.Request().Header.Get("X-Authorization")
	return auth
}

func (ctx *Context) GetToken() string {
	token := ctx.getSessionToken()
	if token == "" {
		token = ctx.getBearerToken()
	}
	return token
}

func (ctx *Context) SUCCESS(v interface{}) error {
	return ctx.JSON(http.StatusOK, map[string]interface{}{
		"status":  0,
		"message": "OK",
		"data":    v,
	})
}

func (ctx *Context) PAGINATION(v interface{}, page, count, total int) error {
	return ctx.SUCCESS(map[string]interface{}{
		"list":  v,
		"total": total,
		"page":  page,
		"count": count,
	})
}

func (ctx *Context) QueryInt(name string) int64 {
	v, err := strconv.Atoi(ctx.QueryParam(name))
	if err != nil {
		return 0
	}
	return int64(v)
}

func (ctx *Context) ParamInt(name string) int64 {
	v, err := strconv.Atoi(ctx.Param(name))
	if err != nil {
		return 0
	}
	return int64(v)
}
