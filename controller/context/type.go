package context

import (
	"github.com/labstack/echo"
	accountModel "gitlab.com/lightim/laser/model/account"
)

type Context struct {
	echo.Context
	Token   *accountModel.Token
	Account *accountModel.Account
}
