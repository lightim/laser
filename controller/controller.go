package controller

import (
	"net/http"

	"github.com/go-playground/validator"
	"github.com/labstack/echo"
	"gitlab.com/lightim/laser/controller/account"
	"gitlab.com/lightim/laser/controller/file"
	"gitlab.com/lightim/laser/controller/friend"
	"gitlab.com/lightim/laser/controller/middleware"
	"gitlab.com/lightim/laser/controller/system"
	"gitlab.com/lightim/laser/errors"
)

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}

func errorHandler(err error, c echo.Context) {
	if e, ok := err.(*errors.Error); ok {
		c.JSON(e.HttpStatus, e)
	} else if e, ok := err.(*echo.HTTPError); ok {
		c.JSON(e.Code, errors.NewError(0, -1, e.Message))
	} else {
		c.JSON(http.StatusInternalServerError, errors.NewError(0, -1, err.Error()))
	}
}

func Register(e *echo.Echo) {
	e.Validator = &CustomValidator{validator: validator.New()}
	e.HTTPErrorHandler = errorHandler
	api := e.Group("/api", middleware.ContextMiddleware)
	authAPI := api.Group("", middleware.AuthMiddleware)

	api.GET("/system", system.GetSystemConfig)

	api.POST("/file", file.UploadFile)
	api.GET("/file/:id", file.DownloadFile)

	/* 注册登录 */
	api.POST("/account", account.Signup)
	api.PUT("/account", account.Login)

	authAPI.GET("/account", account.GetAccount)
	authAPI.GET("/account/search", account.SearchAccount)
	authAPI.GET("/user", account.GetUser)
	authAPI.GET("/friends", friend.FindUserFriends)
	authAPI.POST("/friend/request", friend.CreateFriendRequest)
	authAPI.PUT("/friend/request/accept", friend.AcceptFriendRequest)
	authAPI.GET("/friend/request/:id", friend.GetUserFriendRequest)
}
