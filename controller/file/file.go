package file

import (
	"fmt"
	"io"
	"net/http"
	"os"

	"github.com/labstack/echo"
	"gitlab.com/lightim/laser/controller/context"
	"gitlab.com/lightim/laser/errors"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func UploadFile(c echo.Context) error {
	ctx := c.(*context.Context)

	ff, err := ctx.FormFile("file")
	if err != nil {
		fmt.Println(err)
		return errors.ErrRequestInvalid
	}
	f, err := ff.Open()
	if err != nil {
		fmt.Println(err)
		return errors.ErrRequestInvalid
	}
	defer f.Close()

	fileid := primitive.NewObjectID().Hex()
	sf, err := os.Create("./file/" + fileid)
	if err != nil {
		return errors.ErrInternalException
	}
	defer sf.Close()

	if _, err := io.Copy(sf, f); err != nil {
		return errors.ErrInternalException
	}
	return ctx.SUCCESS(map[string]interface{}{
		"fileid": fileid,
	})
}

func DownloadFile(c echo.Context) error {
	ctx := c.(*context.Context)
	fileid := c.Param("id")

	f, err := os.Open("./file/" + fileid)
	if err != nil {
		return errors.ErrFileNotFound
	}
	defer f.Close()

	return ctx.Stream(http.StatusOK, "image/jpeg", f)
}
