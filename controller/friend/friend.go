package friend

import (
	"fmt"

	"github.com/labstack/echo"
	"gitlab.com/lightim/laser/controller/context"
	"gitlab.com/lightim/laser/electron"
	"gitlab.com/lightim/laser/errors"
	accountModel "gitlab.com/lightim/laser/model/account"
	friendModel "gitlab.com/lightim/laser/model/friend"
	"gitlab.com/lightim/light/log"
	"gitlab.com/lightim/light/proto"
)

/* 获取用户的好友列表 */
func FindUserFriends(c echo.Context) error {
	ctx := c.(*context.Context)

	friends, err := friendModel.FindUserFriends(ctx.Account.Id)
	if err != nil {
		return errors.ErrInternalException
	}
	return ctx.SUCCESS(friends)
}

/* 获取好友请求列表 */
func GetUserFriendRequest(c echo.Context) error {
	ctx := c.(*context.Context)

	id := ctx.ParamInt("id")
	if id <= 0 {
		return errors.ErrRequestInvalid
	}

	freq, err := friendModel.GetUserFriendRequest(id, ctx.Account.Id)
	if err != nil {
		return errors.ErrInternalException
	} else if freq == nil {
		return errors.ErrFriendRequestNotFound
	}
	return ctx.SUCCESS(freq)
}

func CreateFriendRequest(c echo.Context) error {
	ctx := c.(*context.Context)

	var req CreateFriendRequestRequest
	if err := ctx.Bind(&req); err != nil {
		return errors.ErrRequestInvalid
	} else if err := ctx.Validate(&req); err != nil {
		return errors.ErrRequestInvalid
	}

	a, err := accountModel.GetAccountById(req.UserID)
	if err != nil {
		return errors.ErrInternalException
	} else if a == nil {
		return errors.ErrAccountNotFound
	}

	freq, err := friendModel.CreateUserFriendRequest(ctx.Account.Id, req.UserID, req.Message, req.Remark)
	if err != nil {
		return errors.ErrInternalException
	}
	if err := electron.SendNotification(
		fmt.Sprintf("%d", req.UserID),
		int(proto.Notification_FriendRequest),
		map[string]string{
			"from_id":       fmt.Sprintf("%d", ctx.Account.Id),
			"from_nickname": ctx.Account.Nickname,
			"from_avatar":   ctx.Account.Avatar,
			"message":       freq.Message,
			"id":            fmt.Sprintf("%d", freq.ID),
		},
		true); err != nil {
		log.Errorf("SendNotification Error: %v", err)
	}
	return ctx.SUCCESS(freq)
}

func AcceptFriendRequest(c echo.Context) error {
	ctx := c.(*context.Context)

	var req AcceptFriendRequestRequest
	if err := ctx.Bind(&req); err != nil {
		return errors.ErrRequestInvalid
	} else if err := ctx.Validate(&req); err != nil {
		return errors.ErrRequestInvalid
	}

	freq, err := friendModel.AcceptUserFriendRequest(req.ID, ctx.Account.Id)
	if err != nil {
		return errors.ErrInternalException
	} else if freq == nil {
		return errors.ErrFriendRequestNotFound
	}

	if err := electron.SendNotification(fmt.Sprintf("%d", freq.UserID), int(proto.Notification_FriendReply), map[string]string{
		"friend_id":       fmt.Sprintf("%d", ctx.Account.Id),
		"friend_nickname": ctx.Account.Nickname,
		"friend_avatar":   ctx.Account.Avatar,
	}, true); err != nil {
		log.Errorf("SendNotification Error: %v", err)
	}

	if err := electron.SendContentMessage(fmt.Sprintf("%d", freq.UserID), fmt.Sprintf("%d", freq.FriendID), int(proto.ContentMessage_Text), map[string]string{
		"text": freq.Message,
	}); err != nil {
		log.Errorf("SendContentMessage Error: %v", err)
	}
	return ctx.SUCCESS(nil)
}
