package friend

type CreateFriendRequestRequest struct {
	UserID  int64  `json:"user_id" form:"user_id" query:"user_id" validate:"gt=0"`
	Message string `json:"message" form:"message" query:"message"`
	Remark  string `json:"remark" form:"remark" query:"remark"`
}

type AcceptFriendRequestRequest struct {
	ID int64 `json:"id" form:"id" query:"id" valdiate:"gt=0"`
}
