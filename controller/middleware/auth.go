package middleware

import (
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/lightim/laser/controller/context"
	"gitlab.com/lightim/laser/errors"
	accountModel "gitlab.com/lightim/laser/model/account"
)

func AuthMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.(*context.Context)
		t := ctx.GetToken()
		if t == "" {
			return c.NoContent(http.StatusUnauthorized)
		}
		token, err := accountModel.GetToken(t)
		if err != nil {
			return errors.ErrInternalException
		} else if token == nil {
			return c.NoContent(http.StatusUnauthorized)
		}
		account, err := accountModel.GetAccountById(token.UserID)
		if err != nil {
			return errors.ErrInternalException
		} else if account == nil {
			return c.NoContent(http.StatusUnauthorized)
		}
		ctx.Token = token
		ctx.Account = account
		return next(ctx)
	}
}
