package middleware

import (
	"github.com/labstack/echo"
	"gitlab.com/lightim/laser/controller/context"
)

func ContextMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := &context.Context{
			Context: c,
		}
		return next(ctx)
	}
}
