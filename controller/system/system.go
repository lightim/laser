package system

import (
	"github.com/labstack/echo"
	"gitlab.com/lightim/laser/controller/context"
	"gitlab.com/lightim/laser/electron"
)

func GetSystemConfig(c echo.Context) error {
	ctx := c.(*context.Context)

	appId := electron.GetAppId()

	cfg := map[string]interface{}{
		"appId": appId,
	}

	return ctx.SUCCESS(cfg)
}
