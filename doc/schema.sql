CREATE DATABASE "laser" WITH ENCODING=UTF8;
\c "laser";

CREATE TABLE "account" (
    "id" BIGSERIAL PRIMARY KEY,
    "email" VARCHAR(64) NOT NULL,
    "username" VARCHAR(128) NOT NULL,
    "salt" VARCHAR(32) NOT NULL,
    "password" VARCHAR(128) NOT NULL,
    "ptype" VARCHAR(16) NOT NULL,
    "nickname" VARCHAR(128) NOT NULL,
    "avatar" VARCHAR(1024) NOT NULL,
    "gender" SMALLINT NOT NULL DEFAULT 0,
    "email_verified" BOOLEAN NOT NULL DEFAULT FALSE,
    "utime" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    "ctime" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    UNIQUE("email"),
    UNIQUE("username")
);
CREATE INDEX ON "account"("nickname");
COMMENT ON TABLE "account" IS '帐号表';
COMMENT ON COLUMN "account"."email" IS '注册邮箱';
COMMENT ON COLUMN "account"."username" IS '用户名';
COMMENT ON COLUMN "account"."password" IS '加密后的密码';
COMMENT ON COLUMN "account"."ptype" IS '密码的加密方式。MD5、SHA256等';
COMMENT ON COLUMN "account"."nickname" IS '用户昵称';
COMMENT ON COLUMN "account"."avatar" IS '用户头像';
COMMENT ON COLUMN "account"."gender" IS '性别。0-未指定，1-男，2-女';
COMMENT ON COLUMN "account"."email_verified" IS '是否验证了邮箱';
COMMENT ON COLUMN "account"."utime" IS '更新时间';
COMMENT ON COLUMN "account"."ctime" IS '创建时间';

CREATE TABLE "token" (
    "id" BIGSERIAL PRIMARY KEY,
    "user_id" BIGINT NOT NULL,
    "token" VARCHAR(128) NOT NULL,
    "etime" TIMESTAMP WITH TIME ZONE,
    "utime" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    "ctime" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    UNIQUE("token")
);
CREATE INDEX ON "token"("user_id");
COMMENT ON TABLE "token" IS '用户登录令牌';
COMMENT ON COLUMN "token"."user_id" IS '用户Id';
COMMENT ON COLUMN "token"."token" IS '登录token';
COMMENT ON COLUMN "token"."etime" IS '过期时间，为空则表示没有过期时间';
COMMENT ON COLUMN "token"."utime" IS '更新时间';
COMMENT ON COLUMN "token"."ctime" IS '创建时间';

CREATE TABLE "user_friend" (
    "id" BIGSERIAL PRIMARY KEY,
    "user_id" BIGINT NOT NULL,
    "friend_id" BIGINT NOT NULL,
    "utime" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    "ctime" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    UNIQUE("user_id", "friend_id")
);
COMMENT ON TABLE "user_friend" IS '用户好友关系';
COMMENT ON COLUMN "user_friend"."user_id" IS '用户ID';
COMMENT ON COLUMN "user_friend"."friend_id" IS '好友ID';
COMMENT ON COLUMN "user_friend"."utime" IS '更新时间';
COMMENT ON COLUMN "user_friend"."ctime" IS '创建时间';

CREATE TABLE "user_to_user_setting" (
    "id" BIGSERIAL PRIMARY KEY,
    "user_id" BIGINT NOT NULL,
    "user_id2" BIGINT NOT NULL,
    "remark" VARCHAR(64) NOT NULL DEFAULT '',
    "utime" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    "ctime" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    UNIQUE("user_id","user_id2")
);
CREATE INDEX ON "user_to_user_setting"("user_id");
COMMENT ON TABLE "user_to_user_setting" IS '用户1对用户2的设置';
COMMENT ON COLUMN "user_to_user_setting"."user_id" IS '用户ID';
COMMENT ON COLUMN "user_to_user_setting"."user_id2" IS '对象ID';
COMMENT ON COLUMN "user_to_user_setting"."remark" IS '备注';
COMMENT ON COLUMN "user_to_user_setting"."utime" IS '更新时间';
COMMENT ON COLUMN "user_to_user_setting"."ctime" IS '创建时间';


CREATE TABLE "user_friend_request" (
    "id" BIGSERIAL PRIMARY KEY,
    "user_id" BIGINT NOT NULL,
    "friend_id" BIGINT NOT NULL,
    "message" VARCHAR(128) NOT NULL DEFAULT '',
    "remark" VARCHAR(32) NOT NULL DEFAULT '',
    "status" SMALLINT NOT NULL DEFAULT 0,
    "utime" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    "ctime" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);
CREATE INDEX ON "user_friend_request"("user_id", "friend_id","status");
COMMENT ON TABLE "user_friend_request" IS '好友请求';
COMMENT ON COLUMN "user_friend_request"."user_id" IS '发送方ID';
COMMENT ON COLUMN "user_friend_request"."friend_id" IS '接受方ID';
COMMENT ON COLUMN "user_friend_request"."message" IS '验证消息';
COMMENT ON COLUMN "user_friend_request"."remark" IS '好友备注';
COMMENT ON COLUMN "user_friend_request"."status" IS '请求状态。0-未处理，1-已接受，2-已拒绝。';
COMMENT ON COLUMN "user_friend_request"."utime" IS '更新时间';
COMMENT ON COLUMN "user_friend_request"."ctime" IS '创建时间';