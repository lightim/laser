package electron

type Electron struct {
	Host   string `json:"host"`
	AppId  string `json:"app_id"`
	AppKey string `json:"app_key"`
}

var (
	e *Electron
)

func Init(host, appId, appKey string) error {
	e = &Electron{
		Host:   host,
		AppId:  appId,
		AppKey: appKey,
	}
	return nil
}

func CreateToken(userId, client, token string) error {
	data := map[string]interface{}{
		"user_id": userId,
		"client":  client,
		"token":   token,
	}

	return post("/user/token", data)
}

func SendNotification(userId string, t int, content map[string]string, needAck bool) error {
	data := map[string]interface{}{
		"user_id":  userId,
		"type":     t,
		"content":  content,
		"need_ack": needAck,
	}

	return post("/message/notification", data)
}

func SendContentMessage(fromUserId, toUserId string, t int, content map[string]string) error {
	data := map[string]interface{}{
		"from_user_id": fromUserId,
		"to_user_id":   toUserId,
		"type":         t,
		"content":      content,
	}
	return post("/message/contentmessage", data)
}

func GetAppId() string {
	return e.AppId
}

func AddUserFriend(userId, friendId string) error {
	data := map[string]interface{}{
		"user_id":   userId,
		"friend_id": friendId,
	}

	return post("/user/friend", data)
}
