package errors

import (
	"fmt"
	"net/http"
)

/* 这里的错误全部都是对客户端的 */
var (
	ErrInternalException = NewError(http.StatusInternalServerError, -1, "服务器错误")
	ErrSourceInvalid     = NewError(http.StatusBadRequest, -2, "客户端未定义")
	ErrRequestInvalid    = NewError(http.StatusBadRequest, -3, "参数错误")

	/* 帐号相关 */
	ErrEmailAlreadyExists    = NewError(http.StatusOK, 10001, "邮箱已存在")
	ErrUsernameAlreadyExists = NewError(http.StatusOK, 10002, "用户名已存在")
	ErrAccountNotFound       = NewError(http.StatusOK, 10003, "帐号不存在")
	ErrPasswordIncorrect     = NewError(http.StatusOK, 10004, "密码错误")
	ErrEmailMalformed        = NewError(http.StatusOK, 10005, "邮箱格式错误")
	ErrPasswordMalformed     = NewError(http.StatusOK, 10006, "密码过于简单")

	/* 文件不存在  */
	ErrFileNotFound = NewError(http.StatusNotFound, 20001, "文件不存在 ")

	/* 好友 */
	ErrFriendRequestNotFound = NewError(http.StatusOK, 30001, "好友请求不存在")
)

func NewError(status, code int, msg interface{}) *Error {
	return &Error{
		HttpStatus: status,
		Status:     code,
		Message:    fmt.Sprintf("%v", msg),
	}
}

func CopyError(e *Error) *Error {
	err := new(Error)
	*err = *e
	return err
}

func CopyErrorWithMsg(e *Error, msg string) *Error {
	err := CopyError(e)
	err.Message = msg
	return err
}

type Error struct {
	HttpStatus int    `json:"-"`
	Status     int    `json:"status"`
	Message    string `json:"message"`
}

func (e *Error) Error() string {
	return e.Message
}
