package account

import (
	"gitlab.com/lightim/laser/model"
	"gitlab.com/lightim/light/log"
	"gitlab.com/lightim/light/x"
)

func GetAccountByEmail(email string) (*Account, error) {
	db := model.DB()

	var account Account
	if err := db.Model(&account).Where("email=?", email).Select(); err != nil {
		if err != model.ErrNoRows {
			log.Errorf("GetAccountById Error: %v", err)
			return nil, err
		}
		return nil, nil
	}
	return &account, nil
}

func GetAccountByUsername(username string) (*Account, error) {
	db := model.DB()

	var account Account
	if err := db.Model(&account).Where("username=?", username).Select(); err != nil {
		if err != model.ErrNoRows {
			log.Errorf("GetAccountById Error: %v", err)
			return nil, err
		}
		return nil, nil
	}
	return &account, nil
}

func GetAccountById(id int64) (*Account, error) {
	db := model.DB()

	var account Account
	if err := db.Model(&account).Where("id=?", id).Select(); err != nil {
		if err != model.ErrNoRows {
			log.Errorf("GetAccountById Error: %v", err)
			return nil, err
		}
		return nil, nil
	}
	return &account, nil
}

func CreateAccount(email, plain, nickname, avatar string) (*Account, error) {
	db := model.DB()
	account := Account{
		Email:    email,
		Username: "bid_" + x.UUID(),
		Salt:     x.RandStringRunes(12),
		PType:    PTypeSHA256,
		Nickname: nickname,
		Avatar:   avatar,
	}
	account.Password = account.EncryptPassword(plain)
	if err := db.Insert(&account); err != nil {
		log.Errorf("CreateAccount Error: %v", err)
		return nil, err
	}
	return &account, nil
}

func SearchAccountByEmailOrUsername(q string) ([]*Account, error) {
	db := model.DB()

	accounts := make([]*Account, 0)
	if err := db.Model(&accounts).Where(`"email"=? OR "username"=?`, q, q).ExcludeColumn(`email`).Select(); err != nil {
		log.Errorf("SearchAccounts Error: %v", err)
		return nil, err
	}
	return accounts, nil
}
