package account

import (
	"gitlab.com/lightim/laser/model"
	"gitlab.com/lightim/light/log"
)

func GetUser2UserSetting(userID, userID2 int64) (*User2UserSetting, error) {
	db := model.DB()

	var s User2UserSetting
	if err := db.Model(&s).Where("user_id=? AND user_id2=?", userID, userID2).Select(); err != nil {
		if err != model.ErrNoRows {
			log.Errorf("GetUser2UserSetting Error: %v", err)
			return nil, err
		}
		return nil, nil
	}
	return &s, nil
}
