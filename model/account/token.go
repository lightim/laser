package account

import (
	"fmt"
	"time"

	"gitlab.com/lightim/laser/electron"
	"gitlab.com/lightim/laser/model"
	"gitlab.com/lightim/light/log"
	"gitlab.com/lightim/light/x"
)

func GetToken(token string) (*Token, error) {
	db := model.DB()

	var t Token
	if err := db.Model(&t).Where("token=?", token).Select(); err != nil {
		if err != model.ErrNoRows {
			log.Errorf("GetToken Error: %v", err)
			return nil, err
		}
		return nil, nil
	}
	return &t, nil
}

func CreateToken(userID int64, etime time.Time) (*Token, error) {
	tx, err := model.DB().Begin()
	if err != nil {
		log.Errorf("pg.Begin Error: %v", err)
		return nil, err
	}
	defer tx.Rollback()

	if _, err := tx.Model(&Token{}).Where("user_id=?", userID).Delete(); err != nil {
		log.Errorf("CreateToken Error: %v", err)
		return nil, err
	}

	t := Token{
		UserID: userID,
		Token:  x.RandStringRunes(5) + x.UUID() + x.RandStringRunes(5),
		ETime:  etime,
	}
	if err := tx.Insert(&t); err != nil {
		log.Errorf("CreateToken Error: %v", err)
		return nil, err
	}

	if err := electron.CreateToken(fmt.Sprintf("%d", userID), "", t.Token); err != nil {
		log.Errorf("CreateToken Error: %v", err)
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		log.Errorf("pg.Commit Error: %v", err)
		return nil, err
	}

	return &t, nil
}
