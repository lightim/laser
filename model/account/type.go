package account

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"fmt"
	"time"
)

const (
	PTypeMD5    = "MD5"
	PTypeSHA1   = "SHA1"
	PTypeSHA256 = "SHA256"
)

type Account struct {
	TableName     struct{}  `sql:"account" json:"-"`
	Id            int64     `sql:"id,pk autoincr" json:"id"`
	Email         string    `sql:"email" json:"email"`
	Username      string    `sql:"username" json:"username"`
	Salt          string    `sql:"salt" json:"-"`
	Password      string    `sql:"password" json:"-"`
	PType         string    `sql:"ptype" json:"-"`
	Nickname      string    `sql:"nickname" json:"nickname"`
	Avatar        string    `sql:"avatar" json:"avatar"`
	Gender        int       `sql:"gender,notnull" json:"gender"`
	EmailVerified bool      `sql:"email_verified,notnull" json:"email_verified"`
	UTime         time.Time `sql:"utime" json:"utime"`
	CTime         time.Time `sql:"ctime" json:"ctime"`
}

func (a *Account) EncryptPassword(plain string) string {
	if plain == "" {
		return ""
	}
	raw := []byte(a.Salt + "$" + plain)
	if a.PType == PTypeMD5 {
		return fmt.Sprintf("%X", md5.New().Sum(raw))
	} else if a.PType == PTypeSHA1 {
		return fmt.Sprintf("%X", sha1.New().Sum(raw))
	} else if a.PType == PTypeSHA256 {
		return fmt.Sprintf("%X", sha256.New().Sum(raw))
	}
	return ""
}

func (a *Account) VerifyPassword(plain string) bool {
	if plain == "" || a.Password == "" {
		return false
	}
	return a.Password == a.EncryptPassword(plain)
}

type Token struct {
	TableName struct{}  `sql:"token,discard_unknown_columns" json:"-"`
	ID        int64     `sql:"id,pk" json:"id"`
	UserID    int64     `sql:"user_id" json:"user_id"`
	Token     string    `sql:"token" json:"token"`
	ETime     time.Time `sql:"etime" json:"etime"`
	UTime     time.Time `sql:"utime" json:"utime"`
	CTime     time.Time `sql:"ctime" json:"ctime"`
}

type User2UserSetting struct {
	TableName struct{}  `sql:"user_to_user_setting,discard_unknown_columns" json:"-"`
	ID        int64     `sql:"id,pk" json:"id"`
	UserID    int64     `sql:"user_id" json:"user_id"`
	UserID2   int64     `sql:"user_id2" json:"user_id2"`
	Remark    string    `sql:"remark,notnull" json:"remark"`
	UTime     time.Time `sql:"utime" json:"utime"`
	CTime     time.Time `sql:"ctime" json:"ctime"`
}
