package friend

import (
	"fmt"
	"time"

	"gitlab.com/lightim/laser/electron"
	"gitlab.com/lightim/laser/model"
	"gitlab.com/lightim/light/log"
)

func GetUserFriendByUserIDAndFriendID(userID, friendID int64) (*UserFriend, error) {
	db := model.DB()

	var uf UserFriend
	if err := db.Model(&uf).Where("user_id=?", userID).Where("friend_id=?", friendID).Select(); err != nil {
		if err != model.ErrNoRows {
			log.Errorf("GetUserFriendByUserIDAndFriendID Error: %v", err)
			return nil, err
		}
		return nil, nil
	}
	return &uf, nil
}

func FindUserFriends(userID int64) ([]*UserFriend, error) {
	db := model.DB()

	friends := make([]*UserFriend, 0)

	if err := db.Model(&friends).Column(`user_friend.*`, `Friend`).Where(`"user_id"=?`, userID).Order(`friend.nickname`).Select(); err != nil {
		log.Errorf("FindUserFriends Error: %v", err)
		return nil, err
	}
	return friends, nil
}

func CreateUserFriendRequest(userID, friendID int64, message, remark string) (*UserFriendRequest, error) {
	db, err := model.DB().Begin()
	if err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	defer db.Rollback()

	req := UserFriendRequest{
		UserID:   userID,
		FriendID: friendID,
	}

	if err := db.Model(&req).Where(`"user_id"=?`, userID).Where(`"friend_id"=?`, friendID).Where(`"status"=?`, UserFriendRequestStatusInitial).Select(); err != nil {
		if err != model.ErrNoRows {
			log.Errorf("CreateUserFriendRequest Error: %v", err)
			return nil, err
		}
		req.Remark = remark
		req.Message = message

		/* 请求不存在，新建 */
		if err := db.Insert(&req); err != nil {
			log.Errorf("CreateUserFriendRequest Error: %v", err)
			return nil, err
		}
	} else {
		/* 已存在，更新 */
		req.Remark = remark
		req.Message = message
		req.UTime = time.Now()
		req.CTime = time.Now()
		if _, err := db.Model(&req).Where(`"user_id"=?`, userID).Where(`"friend_id"=?`, friendID).
			Where(`"status"=?`, UserFriendRequestStatusInitial).Set(`"remark"=?remark`).
			Set(`"message"=?message`).Set(`"utime"=?utime`).Set(`"ctime"=?ctime`).Update(); err != nil {
			log.Errorf("CreateUserFriendRequest Error: %v", err)
			return nil, err
		}
	}

	if err := db.Commit(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}

	return &req, nil
}

func GetUserFriendRequest(id, friendID int64) (*UserFriendRequest, error) {
	db := model.DB()

	req := UserFriendRequest{ID: id}
	if err := db.Model(&req).Where(`"user_friend_request"."id"=?`, id).Where(`"user_friend_request"."friend_id"=?`, friendID).Column(`user_friend_request.*`, `User`).Select(); err != nil {
		if err != model.ErrNoRows {
			log.Errorf("GetUserFriendRequest Error: %v", err)
			return nil, err
		}
		return nil, nil
	}
	return &req, nil
}

func AcceptUserFriendRequest(id, friendID int64) (*UserFriendRequest, error) {
	db, err := model.DB().Begin()
	if err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	defer db.Rollback()

	req := UserFriendRequest{}
	if err := db.Model(&req).Where(`"id"=?`, id).Where(`"friend_id"=?`, friendID).Select(); err != nil {
		if err != model.ErrNoRows {
			log.Errorf("AcceptUserFriendRequest Error: %v", err)
			return nil, err
		}
		return nil, nil
	}

	if _, err := db.Model(&req).WherePK().Set(`"status"=?`, UserFriendRequestStatusAccepted).Update(); err != nil {
		log.Errorf("AcceptUserFriendRequest Error: %v", err)
		return nil, err
	}

	uf := UserFriend{
		UserID:   req.UserID,
		FriendID: req.FriendID,
	}
	if _, err := db.Model(&uf).OnConflict(`DO NOTHING`).Insert(); err != nil {
		log.Errorf("AcceptUserFriendRequest Error: %v", err)
		return nil, err
	}

	uf = UserFriend{
		UserID:   req.FriendID,
		FriendID: req.UserID,
	}
	if _, err := db.Model(&uf).OnConflict(`DO NOTHING`).Insert(); err != nil {
		log.Errorf("AcceptUserFriendRequest Error: %v", err)
		return nil, err
	}

	if err := electron.AddUserFriend(fmt.Sprintf("%d", req.UserID), fmt.Sprintf("%d", req.FriendID)); err != nil {
		log.Errorf("AddUserFriend Error: %v", err)
		return nil, err
	}

	if err := db.Commit(); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	return &req, nil
}
