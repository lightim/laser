package friend

import (
	"time"

	accountModel "gitlab.com/lightim/laser/model/account"
)

type UserFriend struct {
	TableName struct{}  `sql:"user_friend" json:"-"`
	ID        int64     `sql:"id,pk autoincr" json:"id"`
	UserID    int64     `sql:"user_id" json:"user_id"`
	FriendID  int64     `sql:"friend_id" json:"friend_id"`
	UTime     time.Time `sql:"utime" json:"utime"`
	CTime     time.Time `sql:"ctime" json:"ctime"`

	Friend *accountModel.Account `pg:"joinFK:friend_id" json:"friend"`
}

const (
	UserFriendRequestStatusInitial  = 0
	UserFriendRequestStatusDenied   = 1
	UserFriendRequestStatusAccepted = 2
)

type UserFriendRequest struct {
	TableName struct{}  `sql:"user_friend_request,discard_unknown_columns" json:"-"`
	ID        int64     `sql:"id,pk autoincr" json:"id"`
	UserID    int64     `sql:"user_id" json:"user_id"`
	FriendID  int64     `sql:"friend_id" json:"friend_id"`
	Message   string    `sql:"message,notnull" json:"message"`
	Remark    string    `sql:"remark,notnull" json:"remark"`
	Status    int       `sql:"status" json:"status"`
	UTime     time.Time `sql:"utime" json:"utime"`
	CTime     time.Time `sql:"ctime" json:"ctime"`

	User *accountModel.Account `pg:"joinFK:user_id" json:"user"`
}
