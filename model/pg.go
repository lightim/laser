package model

import (
	"fmt"

	"github.com/go-pg/pg"
)

var _db *pg.DB = nil

var ErrNoRows = pg.ErrNoRows

func DB() *pg.DB {
	return _db
}

type dbLogger struct{}

func (d dbLogger) BeforeQuery(q *pg.QueryEvent) {}

func (d dbLogger) AfterQuery(q *pg.QueryEvent) {
	query, err := q.FormattedQuery()
	if err != nil {
		panic(err)
	}
	/* 打印SQL执行事件 */
	fmt.Printf("\033[34m%s\n\033[0m", query)
}

/* 初始化PostgreSQL */
func Init(pgURL string, debug bool) error {
	opts, err := pg.ParseURL(pgURL)
	if err != nil {
		return err
	}
	_db = pg.Connect(opts)
	/* DEBUG */
	if debug{
		_db.AddQueryHook(dbLogger{})
	}
	
	return nil
}
